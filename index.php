<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>PHPit Database Populator</title>
</head>

<body>
<table width="100%" border="1" cellpadding="10">
  <tr>
    <td>
    	<?php 
			if(!isset($_POST['db-host'])){
			#If the user hasn't told us the database info, they need to enter it now.
		?>
    	<form method="post" action="">
        	<label for = "db-host">Host:</label>
            <input type = "text" name = "db-host" placeholder="Enter database host... (ex: localhost)" />
            <br />
            <label for = "db-user">Database User:</label>
            <input type = "text" name = "db-user" placeholder="Enter database username... (ex: root)" />
     	 	<br />
          	<label for = "db-pwd">Database Password:</label>
            <input type = "password" name = "db-pwd" placeholder="Enter password for user... (ex: password)" />
            <br />
            <label for = "db-name">Database Name:</label>
            <input type = "text" name = "db-name" placeholder="Enter database name... (ex: phpit)" />
            <br />
            <input type="submit" name="install-db" id="install-db" value="Populate Database">
   	 	</form>
        <?php 
			}
			else {
				#database connection stuff
				$db_host = htmlspecialchars($_POST['db-host'], ENT_QUOTES);
				$db_name = htmlspecialchars($_POST['db-name'], ENT_QUOTES);
				$db_user = htmlspecialchars($_POST['db-user'], ENT_QUOTES);
				$db_pwd = htmlspecialchars($_POST['db-pwd'], ENT_QUOTES);

				$conn = mysql_connect($db_host, $db_user, $db_pwd);
				if(!$conn){
					die(mysql_error());
				}
				mysql_select_db($db_name) or die(mysql_error());

				#what's the date, dear?
				$date = htmlspecialchars(date('jS \of F Y h:i:s A'), ENT_QUOTES );

				#5 links with which to populate the database.
				$links = array(
					1 => "INSERT INTO links (id, title, link, date, votecount, author) VALUES ('NULL', 'A link!', 'http://google.com', '$date', '1', 'Admin')",
					2 => "INSERT INTO links (id, title, link, date, votecount, author) VALUES ('NULL', 'Another link!', 'http://bitbucket.com/Allan_E', '$date', '1', 'Not_Admin')",
					3 => "INSERT INTO links (id, title, link, date, votecount, author) VALUES ('NULL', 'A Third link!', 'http://reddit.com', '$date', '1', 'Not_Admin')",
					4 => "INSERT INTO links (id, title, link, date, votecount, author) VALUES ('NULL', 'A Fourth link!', 'http://twitter.com', '$date', '1', 'Admin')",
					5 => "INSERT INTO links (id, title, link, date, votecount, author) VALUES ('NULL', 'A Fifth link!', 'http://reddit.com/r/pics', '$date', '1', 'Admin')"
				);
				#time to query the database. Let's hope everything works.
				foreach($links as $data){
					mysql_query($data) or die(mysql_error());
				}

				#A comment for each link.
				$comments = array(
					1 => "INSERT INTO comments (pid, author, comment, date) VALUES ('1', 'Admin', 'This is a comment', '$date')",
					2 => "INSERT INTO comments (pid, author, comment, date) VALUES ('2', 'Admin', 'This is another comment', '$date')",
					3 => "INSERT INTO comments (pid, author, comment, date) VALUES ('3', 'Not_Admin', 'By now you should know what a comment is', '$date')",
					4 => "INSERT INTO comments (pid, author, comment, date) VALUES ('4', 'Not_Admin', 'Proper grammar is reccomended', '$date')",
					5 => "INSERT INTO comments (pid, author, comment, date) VALUES ('5', 'Admin', 'Shameless self-plug', '$date')",
				);
				foreach($comments as $data){
					mysql_query($data) or die(mysql_error());
				}
				$users = array(
					1 => "INSERT INTO users (id, username, password, email, points, emailconfirmed) VALUES ('NULL', 'Admin', MD5('password'), 'NULL', '0', '0')",
					2 => "INSERT INTO users (id, username, password, email, points, emailconfirmed) VALUES ('NULL', 'Not_Admin', MD5('password'), 'NULL', '0', '0')"
				);
				foreach($users as $data){
					mysql_query($data) or die(mysql_error());
				}
				#tell the user we've finished.
				echo 'done.';
			}
		?>
    </td>
  </tr>
</table>
</body>
</html>